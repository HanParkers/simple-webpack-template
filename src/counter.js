import { DocumentRef } from "./document-ref";

const selectors = {
	incrementor: ".counter-incrementor",
	aggregator: ".counter-aggregator",
};

export const Counter = () => {
	const { queryNodes, createText } = DocumentRef();

	const states = new Map();
	const aggregators = queryNodes(selectors.aggregator);
	const incrementors = queryNodes(selectors.incrementor);

	const createState = () => ({ count: 0 });

	const createScope = (scope) => {
		const state = createState();
		states.set(scope, state);
	};

	const getScope = (node) => node.dataset.scope;

	const getState = (scope) => states.get(scope);

	const updateAggregator = (aggregator, state) => {
		const text = createText(`Count: ${state.count}`);

		aggregator.innerHTML = "";
		aggregator.append(text);
	};

	const updateState = (scope, state) => {
		states.set(scope, state);
		aggregators
			.filter((node) => node.dataset.scope === scope)
			.forEach((aggregator) => updateAggregator(aggregator, state));
	};

	const onIncrementorClick = (event) => {
		const scope = getScope(event.target);
		const state = getState(scope);
		const newState = { count: state.count + 1 };

		updateState(scope, newState);
	};

	const create = () => {
		incrementors.forEach((node) => createScope(getScope(node)));
		incrementors.forEach((node) => node.addEventListener("click", onIncrementorClick));
	};

	const destroy = () => {
		states.clear();
		incrementors.forEach((node) => node.removeEventListener("click", onIncrementorClick));
	};

	create();

	return Object.freeze({
		create,
		destroy,
	});
};
