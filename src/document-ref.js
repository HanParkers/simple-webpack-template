import { Collection } from "./collection";

export const DocumentRef = () => {
	if (!window.document) {
		throw new Error("The document is not available.");
	}

	const queryNode = (selector) => Collection(document.querySelector(selector));
	const queryNodes = (selector) => Collection(document.querySelectorAll(selector));

	const createText = (text) => document.createTextNode(text);

	const createNode = (name) => document.createNode(name);

	return Object.freeze({
		document: () => document,
		queryNode,
		queryNodes,
		createText,
		createNode,
	});
};
