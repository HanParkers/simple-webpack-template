const IGNORE_PROTOTYPES = ["constructor", "length"];

export const Collection = (iterable) => {
	const items = [...iterable];

	return Object.freeze({
		...Object.fromEntries(
			Object.getOwnPropertyNames(Array.prototype)
				.filter((name) => !IGNORE_PROTOTYPES.includes(name))
				.map((name) => [name, Array.prototype[name].bind(items)])
		),
		...Object.defineProperties({}, {
			length: {
				get: () => items.length,
				enumerable: false,
				configurable: false,
			}
		}),
		items,
	});
};
