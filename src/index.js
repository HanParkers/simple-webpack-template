import "./styles/index.scss";

import { of, tap } from "rxjs";

import { Counter } from "./counter";

const config = {
	features: {
		counter: true,
	}
};

const init = () => {
	return of(config).pipe(tap(() => console.log("App successfully started!")));
};

init()
	.pipe(
		tap((config) => config.features.counter && Counter())
	)
	.subscribe();
