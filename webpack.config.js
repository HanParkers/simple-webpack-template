const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const devMode = process.env.NODE_ENV !== "production";

module.exports = {
	output: {
		clean: true,
		assetModuleFilename: "assets/[name][ext][query]"
	},
	plugins: [new MiniCssExtractPlugin()],
	module: {
		rules: [
			{
				test: /\.s[ac]ss$/i,
				use: [
					devMode ? "style-loader" : MiniCssExtractPlugin.loader,
					"css-loader",
					"sass-loader",
				]
			},
			{
				test: /\.(png|jpg|gif)$/i,
				dependency: { not: ["url"] },
				use: [
					{
						loader: "url-loader",
						options: {
							limit: 8192,
						}
					},
				],
			},
		],
	},
};
